from . import start_application


app = start_application()


def start():
    import uvicorn

    uvicorn.run("gimkana.main:app", reload=True)
