from fastapi import APIRouter
from fastapi.responses import HTMLResponse
from .templating import templates
from fastapi import Request


ui_router = APIRouter()


@ui_router.get("/", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("base.html", {"request": request})
