from fastapi import FastAPI


def collect_routers(app: FastAPI):
    from .app.ui import ui_router

    app.include_router(ui_router)


def start_application():
    from .config import settings

    app = FastAPI(
        title=settings.PROJECT_NAME, version=settings.PROJECT_VERSION
    )

    collect_routers(app)

    return app
