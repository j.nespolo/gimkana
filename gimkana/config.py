from os import getenv
from typing import Optional

from dotenv import load_dotenv
from pydantic import BaseSettings


load_dotenv(verbose=True)


class Settings(BaseSettings):
    PROJECT_NAME: str = "gimkana"
    PROJECT_VERSION: str = "0.1.0"

    MONGODB_USER = getenv("MONGODB_USER", "root")
    MONGODB_PASSWORD = getenv("MONGODB_PASSWORD", "example")
    MONGODB_HOST = getenv("MONGODB_HOST", "localhost")
    MONGODB_PORT = int(getenv("MONGODB_PORT", "27017"))
    MONGODB_REPLICASET: Optional[str] = getenv("MONGODB_REPLICASET", None)

    MONGODB_DEFAULT_DB = getenv("MONGODB_DEFAULT_DB", "nep-testing")

    @property
    def MONGODB_URI(self) -> str:
        mongodb_uri = (
            f"mongodb://{self.MONGODB_USER}:{self.MONGODB_PASSWORD}"
            f"@{self.MONGODB_HOST}:{self.MONGODB_PORT}"
        )
        if self.MONGODB_USER != "root":
            mongodb_uri += f"/{self.MONGODB_DEFAULT_DB}"
        return mongodb_uri

    # API PAGINATION
    API_DEFAULT_PAGE_SIZE = int(getenv("API_DEFAULT_PAGE_SIZE", "200"))


settings = Settings()
